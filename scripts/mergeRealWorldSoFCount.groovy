 // Change the file paths before running script
 // sof_rel_file indicates the stack overflow counts for relevant or security APIs:
 //     Set to relevant sof file to get relevant profile and to security sof file to get security profile

def id_mapping_file = "output-april-19/ubcbench-extended-id-api-mapping.csv"
def rw_count_file = "output-april-19/ubcbench-apis-real-world-count.txt"
def sof_rel_file="output-april-19/sof-data/ubcbench-relevant-sof.csv"
def out="output-april-19/real-world-sof-relevant-profile.csv"
def id_mapping = new File(id_mapping_file).readLines()
def real_world_api_count = new File(rw_count_file).readLines()
def sof_rel_profile = new File(sof_rel_file).readLines()

total_bmarks = 201
total_rw = 226459
total_posts = 380802
// total_posts = 317000

id_map = [:]
real_world_count_map = [:]
bmark_count_map = [:]

id_mapping.each {
  entry = it.split('!')
  id_map.put(entry[0],entry[1])
}

println "Created ID Map"

real_world_api_count.each {
  entry = it.split(':')
  apiName = id_map.get(entry[0])
  if(apiName != null)
  real_world_count_map.put(apiName,entry[1])
}

println "Created RW map"

benchmark_count.each {
  entry = it.split(':')
  apiName = id_map.get(entry[0])
  if(apiName != null)
  bmark_count_map.put(apiName,entry[1])
}

println "Created bmark map"

new File(out).withPrintWriter { def writer ->
  writer.println("API!# Benchmarks!# Real-world!# Posts!Benchmarks_norm!rw_norm!posts_norm")
  sof_rel_profile[1..sof_rel_profile.size()-1]
    .each {
      e = it.split(',')
      nm = e[0].replace("!",",")
      sof_count = e[e.length-1] as int
      rw_count = real_world_count_map.get(nm)
      b_count = bmark_count_map.get(nm)
      if(rw_count != null && b_count != null)
        writer.println(nm + "!" + b_count + "!" + rw_count + "!" + sof_count + "!"
          + ((b_count as  int)/total_bmarks*100) + "!" + ((rw_count as int)/total_rw*100) + "!" + (-(sof_count as int)/total_posts*100))
      else println "$nm not found"
    }
}

println "Done!"
