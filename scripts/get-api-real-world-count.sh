#!/bin/bash
# Change the input variables
# Set NUM_FILTERED_APIS to number of filtered APIs

PROFILE_MAPPING_CSV="output-april-19/real-world-ubcbench-api-profile.csv"
OUT="output-april-19/ubcbench-apis-real-world-count.txt"
TMP="output-april-19/tmp-profile.csv"
NUM_FILTERED_APIS=837

if [ -e $OUT ] ; then
  rm $OUT
  echo "cleaned old out files"
fi

grep 'apk,2[3-7]' $PROFILE_MAPPING_CSV > $TMP

for i in {4..$NUM_FILTERED_APIS}
do
   API_ID=`head -1 $PROFILE_MAPPING_CSV | cut -d',' -f$i`
   REAL_WORLD_APK_COUNT=`cut -d',' -f$i $TMP | grep '1' | wc -l`
   echo "$API_ID:$REAL_WORLD_APK_COUNT" >> $OUT
done
echo "done!"
